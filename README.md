# swift-google-map
Using Google Maps SDK for iOS from Swift

An example project demonstrating a bug in Google Maps 1.10.1 -- blank map when launched from suspend in background.  In Xcode, go to Edit Scheme > Run, and select "Launch due to a background fetch event"

## To use this sample

1. Clone this sample
2. Run `pod update` to pull down Google Maps SDK for iOS
3. Bring up Xcode: `open SwiftMaps.xcworkspace`
4. Add your API key to `SwiftMaps/AppDelegate.swift`
