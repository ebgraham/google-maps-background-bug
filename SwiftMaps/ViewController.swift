import UIKit
import GoogleMaps

class ViewController: UIViewController, GMSMapViewDelegate {

    private let globalContext = UnsafeMutablePointer<()>()
    
    @IBOutlet var containerView: UIView!
    private var mapView: GMSMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView = self.buildMapView()
        
        if let map = self.mapView {
            self.containerView.addSubview(map)
        }
    }
    
    private func buildMapView() -> GMSMapView {
        let camera = GMSCameraPosition.cameraWithLatitude(-33.8600, longitude: 151.2094, zoom: 10)
        let map = GMSMapView.mapWithFrame(self.view.bounds, camera: camera)
        map.delegate = self
        map.mapType = kGMSTypeNormal
        map.settings.compassButton = false
        map.settings.myLocationButton = false
        return map
    }
}
